const express = require('express')
const dotenv = require("dotenv")
const cors = require("cors")
const path = require("path")
dotenv.config()
const app = express()

const port = 5019
app.use(cors({
    origin:["http://88.200.63.148:3019"],
    methods:["GET", "POST"],
    credentials:true
}));
  
//Import opur custom modules-controllers
const post= require("./routes/Post")
const users = require("./routes/Users")
const event = require("./routes/Event")
const child = require("./routes/Child")
const location = require("./routes/Location")
const message = require ("./routes/Message")

const cookieParser=require("cookie-parser")
const db = require("./db/conn")
const session = require('express-session')

app.use(cookieParser("somesecret"))
app.use(express.json())

app.use(express.static(path.join(__dirname, "build")))

app.use(express.urlencoded({extended : true}));

app.use(session({
    secret:"somesecret",
    resave:false,
    saveUninitialized:false,
    cookies:{
        expires:60*2
    }
}))


//Routes
app.use('/post', post);
app.use('/users', users);
app.use('/event', event);
app.use('/child', child);
app.use('/location', location);
app.use('/message', message);

// app.get("/",(req,res)=>{
//     res.send("hola")
// })

app.get("/",(req,res)=>{
    res.sendFile(path.join(__dirname, "build", "index.html"))
})
    

///App listening on port
app.listen(process.env.PORT || port, ()=>{
console.log(`Server is running on port: ${process.env.PORT || port}`)
})

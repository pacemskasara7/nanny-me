const mysql = require('mysql2')

//connection with the DB
const conn = mysql.createConnection({
    host: process.env.DB_HOST,
    user: process.env.DB_USER,
    password: process.env.DB_PASS, 
    database: process.env.DB_DATABASE,
  })

  //variable to store result from the query
  let dataPool={}

  //Posts
  dataPool.allPosts=()=>{
    return new Promise ((resolve, reject)=>{
      conn.query(`SELECT * FROM Post`, (err,res)=>{
        if(err){return reject(err)}
        return resolve(res)
      })
    })
  };

  dataPool.createPost=(name_child,date,time_from,time_to,description, user_email )=>{
    return new Promise ((resolve, reject)=>{
      conn.query(`INSERT INTO Post (name_child,date,time_from,time_to,description, user_email) VALUES (?,?,?,?,?,?)`, [name_child,date,time_from,time_to,description,user_email], (err,res)=>{
        if(err){return reject(err)}
        return resolve(res)
      })
    })
  };

  //User
  dataPool.profileUser=(email)=>{
    return new Promise ((resolve, reject)=>{
      conn.query(`SELECT * FROM User WHERE email = ? `,[email], (err,res)=>{
        if(err){return reject(err)}
        return resolve(res)
      })
    })
  };


  dataPool.AuthUser=(email)=>
  {
    return new Promise ((resolve, reject)=>{
      conn.query(`SELECT * FROM User WHERE email = ?`, email, (err,res, fields)=>{
        if(err){return reject(err)}
        return resolve(res)
      })
    })   
  };

  dataPool.AddUser=(email,name,phone_number,date_of_birth,password)=>{
    return new Promise ((resolve, reject)=>{
      conn.query(`INSERT INTO User (email,name,phone_number,date_of_birth,password) VALUES (?,?,?,?,?)`, [email,name,phone_number,date_of_birth,password], (err,res)=>{
        if(err){return reject(err)}
        return resolve(res)
      })
    })
  }

  dataPool.allUsers = () =>{
    return new Promise ((resolve, reject)=>{
      conn.query(`SELECT * FROM User`, (err,res)=>{
        if(err){return reject(err)}
        return resolve(res)
      })
    })
  }

  //location
  dataPool.checkExistingLoc = (street,number,city,description)=>{
    return new Promise ((resolve, reject)=>{
      conn.query(`SELECT id FROM Location WHERE (street,number,city,description) = (?,?,?,?) LIMIT 1`, [street,number,city,description], (err,res)=>{
        if(err){return reject(err)}
        return resolve(res)
      })
    })
  }

  dataPool.addLocation = (street,number,city,description)=>{
    return new Promise ((resolve, reject)=>{
      conn.query(`INSERT INTO Location (street,number,city,description) VALUES (?,?,?,?) `, [street,number,city,description], (err,res)=>{
        if(err){return reject(err)}
        return resolve(res)
      })
    })
  }

  dataPool.getLocationById = (id) =>{
    return new Promise ((resolve,reject)=>{
      conn.query(`SELECT * FROM Location WHERE id = ?`, [id], (err,res, fields)=>{
        if(err){return reject(err)}
        return resolve(res)
      })
    })
  }

  //event
  dataPool.allEvents=()=>{
    return new Promise ((resolve, reject)=>{
      conn.query(`SELECT * FROM Event`, (err,res)=>{
        if(err){return reject(err)}
        return resolve(res)
      })
    })
  };

  dataPool.createEvent=(title,date,time,description1,user_email,location_id) =>{
    return new Promise ((resolve, reject)=>{
      conn.query(`INSERT INTO Event (title,date,time,description1,user_email,location_id) VALUES (?,?,?,?,?,?) `, [title,date,time,description1,user_email,location_id], (err,res)=>{
        if(err){return reject(err)}
        return resolve(res)
      })
    })
  }

  //child
  dataPool.allChildrenByUser= (email) =>{
    return new Promise ((resolve, reject)=>{
      conn.query(`SELECT * FROM Child_Profile WHERE user_email = ?`,
      [email], (err,res)=>{
        if(err){return reject(err)}
        return resolve(res)
      })
    })
  }

  dataPool.createChildProfile = (name,surname,age,favourite_food,favourite_game, user_email) =>{ 
    return new Promise ((resolve, reject)=>{
      conn.query(`INSERT INTO Child_Profile (name,surname,age,favourite_food,favourite_game, user_email) VALUES (?,?,?,?,?,?) `, [name,surname,age,favourite_food,favourite_game, user_email], (err,res)=>{
        if(err){return reject(err)}
        return resolve(res)
      })
    })
  }

// Message
dataPool.createMessage = (date,text,user_EmailReceive,user_EmailSend) => {
  return new Promise ((resolve, reject)=>{
    conn.query(`INSERT INTO Message (date,text,user_EmailReceive,user_EmailSend) VALUES (?,?,?,?) `, [date,text,user_EmailReceive,user_EmailSend], (err,res)=>{
      if(err){return reject(err)}
      return resolve(res)
    })
  })
}

dataPool.allMessagesByUser = (user_EmailSend) =>{
  return new Promise ((resolve,reject)=>{
    conn.query(`SELECT * FROM Message WHERE user_EmailSend = ?`, [user_EmailSend], (err,res, fields)=>{
      if(err){return reject(err)}
      return resolve(res)
    })
  })
}

dataPool.allMessagesByUsersend = (user_EmailReceive, user_EmailSend) => {
  return new Promise ((resolve,reject)=>{
    conn.query(`SELECT * FROM Message WHERE user_EmailReceive = ? AND user_EmailSend = ?`, [user_EmailReceive, user_EmailSend], (err,res, fields)=>{
      if(err){return reject(err)}
      return resolve(res)
    })
  })
}
 

dataPool.allMessagesByUserreceive = (email1, email2) => {
  return new Promise((resolve, reject) => {
    conn.query(
      `SELECT * FROM Message WHERE (user_EmailSend = ? AND user_EmailReceive = ?) OR (user_EmailSend = ? AND user_EmailReceive = ?)`,
      [email1, email2, email2, email1],
      (err, res, fields) => {
        if (err) {
          return reject(err);
        }
        return resolve(res);
      }
    );
  });
};

dataPool.allMessages = ()=> {
  return new Promise ((resolve, reject)=>{
    conn.query(`SELECT * FROM Message`, (err,res)=>{
      if(err){return reject(err)}
      return resolve(res)
    })
  })
};


conn.connect((err) => {
      if(err){
          console.log("ERROR: " + err.message);
          return;    
      }
      console.log('Connection established');
    })

module.exports= dataPool


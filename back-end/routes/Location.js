const express= require("express")
const location = express.Router();
const DB = require('../db/conn');
// const session = require("express-session");

//get locations from the db
location.get('/', async (req, res) => {
    const locationId = req.query.id; // Access the ID from request parameters
    try {
      // Use the locationId to fetch information about the specific location
      var queryResult = await DB.getLocationById(locationId);
      res.json(queryResult);
      console.log(queryResult);
    } catch (err) {
      console.log(err);
      res.sendStatus(500);
    }
})


module.exports=location
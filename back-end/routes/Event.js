const express= require("express")
const events = express.Router();
const DB = require('../db/conn');
// const session = require("express-session");


//get all events from the db
events.get('/', async (req,res,next)=>{
try{
    var queryResult = await DB.allEvents();
    res.json(queryResult)
    console.log(queryResult)
}
catch(err){
    console.log(err)
    res.sendStatus(500)
    console.log(queryResult)

}
});

//Inserts one new to the database
events.post('/', async (req,res, next)=>{

    let title= req.body.title
    let date = req.body.date
    let time = req.body.time
    let description1 = req.body.description1

    let street = req.body.street
    let number = req.body.number
    let city = req.body.city
    let description = req.body.description


    let user_email = req.session.user[0].email

    if(!user_email ){

        console.log('User is not logged in. ');
        return res.sendStatus(401);
    }
  
      var isAcompletePost= title &&  date &&time && description1 && street && number && city && description
      if (isAcompletePost)
      {
          try{
            let location_id = await getLocId(street, number,city,description)

            if(!location_id){
                location_id = await createLoc (street,number,city,description)
            }

              var queryResult=await DB.createEvent(title,date,time,description1,user_email,location_id)
              if (queryResult.affectedRows) {
                  console.log("New event added!!")
                }
          }
          catch(err){
              console.log(err)
              res.sendStatus(500)
          }    
      }  
      else
      {
       console.log("A field is empty!!")
       res.status(400).send({
        message: "All fields are required!"
    })
      }

      res.end()
  
    
  }) 

//helper function to getLocation ID
async function getLocId(street, number,city, description){
    try{
        const queryLoc = await DB.checkExistingLoc(street,number,city,description)
        if (queryLoc.length > 0){
            return queryLoc[0].id
        }
        return null
    }
    catch(err){
        throw err
    }
}

//helper function to create loaction, if it doesn't exist
async function createLoc(street, number,city, description){
    try{
        const queryLoc = await DB.addLocation(street,number,city,description)
            return queryLoc.insertId
    }
    catch(err){
        throw err
    }
}
  
    
module.exports=events
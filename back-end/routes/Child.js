const express= require("express")
const child = express.Router();
const DB = require('../db/conn');

//get child from the db
child.get('/', async (req,res,next)=>{
    
    const user_email = req.session.user[0].email;
    console.log(user_email);
try{
    var queryResult = await DB.allChildrenByUser(user_email);
    res.json(queryResult)
    console.log(queryResult)
}
catch(err){
    console.log(err)
    res.sendStatus(500)
    console.log(queryResult)

}
});

//Insert new  post to the database
child.post('/', async (req,res, next)=>{
    
    let name = req.body.name
    let surname = req.body.surname
    let age = req.body.age
    let favourite_food = req.body.favourite_food
    let favourite_game = req.body.favourite_game
    let user_email = req.session.user[0].email

    if(!user_email ){

        console.log('User is not logged in. ');
        return res.sendStatus(401);
    }
  
      var isAcompletePost= name && surname && age && favourite_game && favourite_game
      if (isAcompletePost)
      {
          try{
              var queryResult=await DB.createChildProfile(name,surname,age,favourite_food,favourite_game, user_email)
              if (queryResult.affectedRows) {
                  console.log("Child profile created")
                
                }
                res.json(queryResult)
          }
          catch(err){
              console.log(err)
              res.sendStatus(500)
          }    
      }  
      else
      {
       console.log("A field is empty!!")
       res.status(400).send({
        message: "All fields are required!"
    })
      }

      res.end()    
  }) 

module.exports=child
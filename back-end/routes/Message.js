const express= require("express")
const message  = express.Router();
const DB = require('../db/conn');
const session = require("express-session");

message.use(express.json()); 

//get the old messages between 2 users(the user that is logged in and another from the registered.)
message.get('/oldB', async (req,res,next)=>{
    
    const email1 = req.session.user[0].email;
    const email2 =  req.query.email2;
    try{
        var queryResult = await DB.allMessagesByUserreceive(email1, email2);
        res.json(queryResult)
        console.log(queryResult)
    }
    catch(err){
        console.log(err)
        res.sendStatus(500)
        console.log(queryResult)
    
    }
});

//get all messages
message.get('/', async (req,res,next)=>{
    try{
        var queryResult = await DB.allMessages();
        res.json(queryResult)
        console.log(queryResult)
    }
    catch(err){
        console.log(err)
        res.sendStatus(500)
        console.log(queryResult)
    
    }
});

//post new message
message.post('/', async (req,res, next)=> { 
    
    let date = new Date();
    let text = req.body.text;
    let user_EmailReceive = req.body.user_EmailReceive;
    let user_EmailSend = req.session.user[0].email;

    if (!req.session || !req.session.user) {
        return res.status(401).json({ error: "User not logged in" });
    }
    
    var isAcompleteMessage = text && user_EmailReceive
      if (isAcompleteMessage)
      {
          try{
              var queryResult=await DB.createMessage(date,text,user_EmailReceive,user_EmailSend)
              if (queryResult.affectedRows) {
                  console.log("New message added!!")
                 // res.json(queryResult)

                 //variable to save the new message
                 const createdMessage = {
                    id: queryResult.insertId, //insertId because the id is auto-incremented
                    date: date,
                    text: text,
                    user_EmailReceive: user_EmailReceive,
                    user_EmailSend: user_EmailSend,
                  };
                  
                  res.json(createdMessage); // Sending the created message in the response
          
                }
          }
          catch(err){
              console.log(err)
              res.sendStatus(500)
          }    
      }  
      else
      {
       console.log("A field is empty!!")
      }

      res.end()
  }) 

 module.exports = message 
  
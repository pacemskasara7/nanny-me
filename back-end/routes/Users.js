const express= require("express");
const session = require("express-session");
const users = express.Router();
const DB=require('../db/conn.js');


//Checks if user submited both fields, if user exist and if the combiation of user and password matches
users.get('/', async (req,res,next)=>{

    const email = req.session.user[0].email;

    try{
        var queryResult = await DB.profileUser(email);
        res.json(queryResult)
    }
    catch(err){
        console.log(err)
        res.sendStatus(500)
        console.log(queryResult)
    
    }
  
});

users.get('/login',(req,res)=>{

    if(req.session.user) 
    {
    res.send({
         logged:true,
         user:req.session.user
     })
    }
    else
    {
        res.send({logged:false})
    }
  });


//Checks if user submited both fields, if user exist and if the combiation of user and password matches
users.post('/login', async (req, res) => {
    var email = req.body.email;
	var password = req.body.password;
    if (email && password) 
    {
        try
        {
         let queryResult=await DB.AuthUser(email);
        
                if(queryResult.length>0)
                {
                    if(password===queryResult[0].password)
                    {
                        req.session.user=queryResult;
                        console.log('Logged in user:', req.session.user);
                        console.log("SESSION VALID");
                        res.json(queryResult);
                        
                    }
                    else
                    {
                        console.log("INCORRECT PASSWORD");
                        res.send("Incorrect pass")
                    }
                }else 
                {
                 console.log("USER NOT REGISTRED");   
                 res.send("Not registered")
                }
        }
        catch(err){
            console.log(err)
            res.sendStatus(500)
        }    
    }
    else
    {
        console.log("Please enter Username and Password!")
        res.send("Please enter username and password")
    }
    res.end();
});

//Inserts a new user in our database 
users.post('/register', async (req, res) => {
    
    let email = req.body.email
	let name = req.body.name
    let phone_number = req.body.phone_number
    let date_of_birth = req.body.date_of_birth
    let password = req.body.password

    if (email && name && phone_number && date_of_birth && password) 
    {
        try
        {
         let queryResult=await DB.AddUser(email,name,phone_number,date_of_birth,password);
         if (queryResult.affectedRows) {
            console.log("New user added!!")
          }
               
        }
        catch(err){
            console.log(err)
            res.sendStatus(500)
        }    
    }
    else
    {
        console.log("A field is missing!")
        res.send ("A field is missing!")
    }

    res.end();
   
});

//get all registered users
users.get('/register', async (req,res,next)=>{

    try{
        var queryResult = await DB.allUsers();
        res.json(queryResult)
    }
    catch(err){
        console.log(err)
        res.sendStatus(500)
        console.log(queryResult)
    
    }
});

module.exports=users

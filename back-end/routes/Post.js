const express= require("express")
const posts = express.Router();
const DB = require('../db/conn');
const session = require("express-session");

//get all posts from the db
posts.get('/', async (req,res,next)=>{
try{
    var queryResult = await DB.allPosts();
    res.json(queryResult)
    console.log(queryResult)
}
catch(err){
    console.log(err)
    res.sendStatus(500)
    console.log(queryResult)

}
});

//Inserts one new to the database
posts.post('/', async (req,res, next)=> { 
    
    let name_child = req.body.name_child
    let date = req.body.date
    let time_from = req.body.time_from
    let time_to = req.body.time_to
    let description = req.body.description

    if (!req.session || !req.session.user) {
        return res.status(401).json({ error: "User not logged in" });
    }
    
    let user_email = req.session.user[0].email

      var isAcompletePost=name_child && date && time_from && time_to && description
      if (isAcompletePost)
      {
          try{
              var queryResult=await DB.createPost(name_child,date,time_from,time_to,description, user_email)
              if (queryResult.affectedRows) {
                  console.log("New post added!!")
                  res.json(queryResult)
                }
          }
          catch(err){
              console.log(err)
              res.sendStatus(500)
          }    
      }  
      else
      {
       console.log("A field is empty!!")
        res.status(400).send({
            message: "All fields are required!"
        })
      }

      res.end()
    
  }) 
  
    
module.exports=posts
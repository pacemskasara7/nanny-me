# Collaborative Childcare Assistance Information System

**Project for Systems III**
**Authored by: Sara Pachemska**
**August 2023**

## Project Scope

The **Collaborative Childcare Assistance Information System** is designed to connect parents needing urgent childcare help due to work commitments with other parents willing to assist. This system aims to create a platform that enables parents to collaborate, ensuring the safety of their children and helping them carry out their parental duties effectively.

## Benefits

- **Parental Support:** Parents can help each other during critical times, ensuring children are in safe hands.
- **Responsibility Fulfillment:** Parents can meet work and personal obligations confidently, knowing their children are cared for by trusted peers.
- **Financial Savings:** Families in need of childcare assistance can save on expensive alternatives, as the system promotes a cost-effective support network.
- **Social Interaction:** Children have the opportunity to interact with different families, promoting social skills and a sense of belonging.

## Development Stack

- **Back-end:** Node.js
- **Front-end:** React
- **Database:** MySQL

The project utilizes Node.js for efficient server-side processing, React for dynamic front-end interactions, and MySQL for structured data management.

## Functionalities

Users can:

1. **Create an Account:** Register with essential information like email, name, phone number, date of birth, and password.
2. **Login:** Access the system with their email and password.
3. **Create Child Profiles:** Create profiles for each child with details like name, surname, age, favorite game, and favorite food.
4. **View Profile:** Access individual and child profiles within the same interface.
5. **Communicate via Chat:** Connect and assist each other through a chat interface.
6. **Create Posts:** Share assistance needs by creating posts with child's name, date, time, and additional information.
7. **Create Events:** Generate events with details like title, date, time, location, and description.

## Challenges

The most challenging part of the implementation was enabling real-time user communication via chat. Handling continuous data flow, synchronization, and accurate association of messages with users and conversations required careful backend and frontend coordination. Aligning logged-in user emails and retrieving distinct message sets were specific frontend challenges.

## Setup
To launch the app the user should use this link:
http://88.200.63.148:5019



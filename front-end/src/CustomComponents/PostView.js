import React from "react";
import axios from "axios";
import help from "./images/urgent2.png";

class PostView extends React.Component {

  //constructor
  constructor(props) {
    super(props);
    this.state = {
      posts: {}
    };
  }

  QSetViewInParent = (obj) => {
    this.props.QIDFromChild(obj);
  };

  componentDidMount()
  {
    //setting the colors
    document.body.style.backgroundImage = "none";
    document.body.style.backgroundColor = "#f2f3f4";

    //API for the posts
    axios.get('/post')
    .then(response=>{
        console.log(response.data)
      this.setState({
        posts:response.data
      })
    })
  };

  //format date
  formatDateToDDMMYYYY = (date) => {
    const formattedDate = new Date(date).toLocaleDateString('en-GB');
    return formattedDate;
  };

  //format time
  formatTimeToHHMM = (date) => {
    const formattedTime = new Date(date).toLocaleTimeString('en-US', {
      hour12: false,
      hour: '2-digit',
      minute: '2-digit',
    });
    return formattedTime;
  };

  render() {

    //variables
    let data = this.state.posts;
    let user = this.props.data[0].email;

  
    return (
    <div className="row row-cols-1 row-cols-md-3 g-4" style={{padding: "10px"}}>
        
      {data.length > 0 ?
       data.reverse().map((d) => {
      return (
        <div className="col" style={{ margin: "auto", marginTop: "5%"}}>
          <div className="card" style={{height: "470px",border:"none",boxShadow: "0 4px 8px 0 rgba(0, 0, 0, 0.2)"}} >
            <div className="card-body">
              <div style={{display: "flex" , marginBottom: "-20px"}}> 
                  <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="currentColor" className="bi bi-person" viewBox="0 0 16 16">
                     <path d="M8 8a3 3 0 1 0 0-6 3 3 0 0 0 0 6Zm2-3a2 2 0 1 1-4 0 2 2 0 0 1 4 0Zm4 8c0 1-1 1-1 1H3s-1 0-1-1 1-4 6-4 6 3 6 4Zm-1-.004c-.001-.246-.154-.986-.832-1.664C11.516 10.68 10.289 10 8 10c-2.29 0-3.516.68-4.168 1.332-.678.678-.83 1.418-.832 1.664h10Z"/>
                  </svg> 
                  <p style={{fontWeight: "300",marginLeft: "10px"}}>{d.user_email}</p>
              </div>
              <hr></hr>
              <img
                 src={help} 
                  alt="Card Image"
                  style={{ width: "100%", height: "200px" }} 
                />
              <p style={{ fontWeight: "bold" }} className="card-text">
               {" Date:"} {this.formatDateToDDMMYYYY(d.date)}
              </p>
              <p style={{ fontWeight: "bold" }} className="card-text">
                {"Time: "}{this.formatTimeToHHMM(d.time_from)} {"-"} {this.formatTimeToHHMM(d.time_to)}
              </p>
              <p className="card-text">
               Description: {d.description}
              </p>
             
            </div>
          
          {user !== d.user_email ? (
            <div className="account">
              <button type="button" onClick={()=> this.QSetViewInParent({page: "chat2", emailRec: d.user_email, emailSend: d.user_email})}>Send message</button>
            </div> 
          ) : ""}

          </div>
        </div>
          )
          })
          : <small style={{fontWeight: "500"}}>There are currently no posts available!</small>}
    </div>
    );
  }
}

export default PostView;

import React from "react";
import {MDBContainer,MDBRow,MDBCol,MDBCard,MDBCardBody,MDBIcon,MDBBtn,MDBTypography,MDBTextArea,MDBCardHeader,} from "mdb-react-ui-kit";
import axios from "axios";
import personImg from "./images/personChat.png";
import messageSR from "./images/messageSR.png";

class Chat2 extends React.Component {

    //constructor
    constructor(props) {
        super(props);
        this.state = {
          users: {},
          distinctEmails: [],
          message: {},
          getMessage: {},
          lastMessage: "",
          messagesArray: [],
          emailCheck: "",
          messageGET: {},
          prevPageExecuted: false,
        };
      }

    componentDidMount() {
        //let user = this.props.data;
        let currentemail = this.props.data[0].email;
       
        axios.get('/users/register',{withCredentials: true}).then((response) => {
          
          const users = response.data;
          const allEmails = users.map((email) => email.email);
          const distinctEmailsSet = new Set(allEmails);

          distinctEmailsSet.delete(currentemail); // Remove the current user's email from the set
          const distinctEmails = Array.from(distinctEmailsSet);

          this.setState({
            users: users,
            distinctEmails: distinctEmails,
            emailCheck: this.props.paramsEmail
          });

          //check if the preivios page is posts, to get the old messages
          if (this.props.prevPage === 'posts' && !this.state.prevPageExecuted) {
            this.QGetMessage();
            this.setState({
              prevPageExecuted: true,
            });
          }
    });

    axios.get('/message',{withCredentials: true})
    .then((response)=>{
      console.log(response.data)
      const messages = response.data;
      this.setState({
        getMessage: response.data,
        lastMessage: response.data[messages.length - 1]
      })
    })
    console.log(this.state.lastMessage)
   }

    componentDidUpdate(prevProps) {
        if ((this.props.paramsEmail !== prevProps.paramsEmail)){
          this.QGetMessage();
        }
    }

    //get the information for the message
    QGetTextFromField = (e) => {
      this.setState((prevState) => ({
        message: { ...prevState.message, [e.target.name]: e.target.value }
      }));
    };
    

  //received messages 
  QGetMessage = async () =>{
      axios.get('/message/oldB',{withCredentials: true, params: {email2: this.props.paramsEmail}})
          .then((response)=>{
            //console.log( response.data)
            
            const sortedMessages = response.data.sort((a, b) => {
              const timestampA = new Date(a.date).getTime();
              const timestampB = new Date(b.date).getTime();
              return timestampA - timestampB;
            });
      
            this.setState({
              messageGET: sortedMessages,
              emailCheck: this.props.paramsEmail,
            });
          })
          .catch(error => {
            console.error("Error fetching messages:", error);
          });
   }

  QPostMessage = async () => {
    const { text } = this.state.message;

    if (!text.trim()) {
      console.log("Message is empty. Cannot send.");
      return;
    }

    axios.post('/message', {

      text: this.state.message.text,
      user_EmailReceive: this.props.userEmailReceive

    }, { withCredentials: true })
    .then(response => {
      console.log(response.data)
      const newMessage = {
        ...response.data,
        user_EmailSend: this.props.userEmailReceive  
      };

      this.setState(prevState => ({
        message: { ...prevState.message, text: '' },
        messagesArray: [...prevState.messagesArray, newMessage],
      }));
      this.componentDidMount();
    })
    .catch(error => {
      console.error("Error fetching user data:", error);
    });
  };

  formatDateTime = (dateTimeString) => {
    const dateTime = new Date(dateTimeString);
    const options = { month: 'short', day: 'numeric', hour: '2-digit', minute: '2-digit' };
    return dateTime.toLocaleDateString('en-US', options);
  }

  QSetViewInParent = (obj) => {
    this.props.QIDFromChild(obj);
  };

  render(){

    const dataMembers = this.state.distinctEmails;

    return (
      <MDBContainer fluid className="py-5" style={{ backgroundColor: "#eee" }}>
        <MDBRow>

          {/* Members */}
          <MDBCol md="6" lg="5" xl="4" className="mb-4 mb-md-0">
            <h5 className="font-weight-bold mb-3 text-center text-lg-start">
              Members
            </h5>

            {dataMembers.length > 0 ? 
            dataMembers.map((d) => {

            return(
            
            <MDBCard>
              <MDBCardBody>
                <MDBTypography listUnStyled className="mb-0">
                  <li className="p-2 border-bottom" style={{ backgroundColor: "#eee" }}>
                    <a  onClick = {()=> {this.QSetViewInParent({page: "chat2", emailSend: d, emailRec: d}); 
                        this.setState({
                          messagesArray: []
                        })}}
                          href="#" className="d-flex justify-content-between">
                          <div className="d-flex flex-row">
                            <img src={personImg} alt="avatar" className="rounded-circle d-flex align-self-center me-3 shadow-1-strong" width="60" />
                            <div className="pt-1">
                              <p className="fw-bold mb-0">{d}</p>
                            </div>
                          </div>
                    </a>
                  </li>
                </MDBTypography>
              </MDBCardBody>
            </MDBCard>
            )})
          : ""}
          </MDBCol>

          {/* Messages */}
          <MDBCol md="6" lg="7" xl="8">
            <MDBTypography listUnStyled>
          
            {this.state.emailCheck ? (
              <div >
                <small>{this.state.emailCheck}</small>

                {(this.state.messageGET.length > 0 ) ?
                (this.state.messageGET.map((d) => (
                  d.user_EmailSend === this.props.data[0].email ? (
                      //right
                      <div>
                        <li className="d-flex justify-content-between mb-4">
                          <img src={messageSR} alt="avatar" className="rounded-circle d-flex align-self-start me-3 shadow-1-strong" width="60"/>
                          <MDBCard className="w-100">
                              <MDBCardHeader className="d-flex justify-content-between p-3">
                                <p className="fw-bold mb-0">{d.user_EmailSend}</p>
                                <p className="text-muted small mb-0">
                                  {this.formatDateTime(d.date)}
                                </p>
                              </MDBCardHeader>
                              <MDBCardBody>
                                <p className="mb-0">
                                {d.text}
                                </p>
                              </MDBCardBody>
                          </MDBCard>
                        </li>
                      </div>
                  ):(
                    //left
                    <div> 
                      <li className="d-flex justify-content-between mb-4" >
                        <MDBCard className="w-100">
                          <MDBCardHeader className="d-flex justify-content-between p-3">
                            <p class="fw-bold mb-0">{d.user_EmailSend}</p>
                            <p class="text-muted small mb-0">
                              {this.formatDateTime(d.date)}
                            </p>
                          </MDBCardHeader>
                          <MDBCardBody>
                            <p className="mb-0">
                              {d.text}
                            </p>
                          </MDBCardBody>
                        </MDBCard>
                        <img src={messageSR} className="rounded-circle d-flex align-self-start ms-3 shadow-1-strong" width="60"/>
                      </li>
                    </div>
                  
                )
                  ))): "" }

              {this.state.messagesArray.length > 0 ?
              (this.state.messagesArray.map((d) => (
              <li className="d-flex justify-content-between mb-4">
                <img src={messageSR} alt="avatar" className="rounded-circle d-flex align-self-start me-3 shadow-1-strong" width="60"
                />
                <MDBCard className="w-100">
                  <MDBCardHeader className="d-flex justify-content-between p-3">
                    <p className="fw-bold mb-0">{this.state.lastMessage.user_EmailSend}</p>
                    <p className="text-muted small mb-0">
                       {this.formatDateTime(d.date)}
                    </p>
                  </MDBCardHeader>
                  <MDBCardBody>
                    <p className="mb-0">
                    {d.text}
                    </p>
                  </MDBCardBody>
                </MDBCard>
              </li>
              )
              )) : ""}
                
                <div>

                    <li className="bg-white mb-3" >
                      <label htmlFor="messageInput" className="form-label">
                          Message
                        </label>
                        <textarea value={this.state.message.text} name="text" id="messageInput" className="form-control" rows={4} onChange={this.QGetTextFromField}></textarea>
                    </li>

                    <div className="account">
                      <button type = "button" onClick = {()=> {this.QPostMessage()}} style={{width: "160px"}}>
                        Send
                      </button>
                    </div>
                    
                </div>
              </div>

              ): ""}

            </MDBTypography>
          </MDBCol>
        </MDBRow>
      </MDBContainer>
    );
  }
}
export default Chat2;
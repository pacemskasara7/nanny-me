import React from "react";
import event from "./images/sport.jpg";
import problem from "./images/problem.jpg";
import post from "./images/post1.jpg";

class AboutView extends React.Component {

  //colors
  componentDidMount() {
    document.body.style.backgroundImage = "none";
    document.body.style.backgroundColor = "#f2f3f4";
  }

  render() {
    return (

      <div>
          <h2
            style={{
              fontStyle: "normal",
              fontFamily: "Roboto",
              fontSize: "calc(18px + 3.0vw)",
              color: "#3DC9FF",
              textAlign: "center"
            }}
        >
          ABOUT NANNY ME
        </h2>
        <div
          style={{
            fontStyle: "italic",
            fontFamily: "Roboto",
            fontSize: "calc(12px + 1.5vw)",
            color: "black",
            textAlign: "center"
          }}
        >
          "Parents helping other parents."
        </div>
      <div style={{ marginTop: "3%", padding: "10px" }}>
        
        <div className="card-group">
          <div className="card">
            <img className="card-img-top" src={event} alt="Card image cap" />
            <div className="card-body">
              <h5 className="card-title">Event</h5>
              <p className="card-text">
                Family-oriented events provide an opportunity for children and
                parents to bond and create lasting memories together,
                strengthening their relationships and fostering a sense of
                togetherness.
              </p>
            </div>
          </div>
          <div className="card">
            <img className="card-img-top" src={problem} alt="Card image cap" />
            <div className="card-body">
              <h5 className="card-title">Problem</h5>
              <p className="card-text">
                The problem revolves around parents who have simultaneous work
                commitments or obligations elsewhere, but face challenges
                finding affordable or accessible childcare options, resulting in
                adverse effects on their professional and personal lives, as
                well as the development of their children.
              </p>
            </div>
          </div>
          <div className="card">
            <img className="card-img-top" src={post} alt="Card image cap" />
            <div className="card-body">
              <h5 className="card-title">Post</h5>
              <p className="card-text">
                Parental posts allow parents to seek guidance and support from
                others who may have faced similar challenges in finding suitable
                childcare options, providing a space for empathy and practical
                advice.
              </p>
            </div>
          </div>
        </div>
      </div>
      </div>
    );
  }
}

export default AboutView;

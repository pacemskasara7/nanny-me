import React from "react";
import { MDBCol, MDBContainer,MDBRow,MDBCard,MDBCardText,MDBCardBody,MDBCardImage,MDBIcon} from "mdb-react-ui-kit";
import axios from "axios";
import person from "./images/person2.png";
import "./buttonStyles.css";

class ProfileView extends React.Component {
  
  constructor(props) {
    super(props);
    this.state = {
      child: {}
    };
  }

  QSetViewInParent = (obj) => {
    this.props.QIDFromChild(obj);
  };

  componentDidMount()
  {
    
    axios.get('/child', {
      withCredentials: true
    })
    .then(response=>{
      console.log(response.data);
        this.setState(
          {
            child: response.data
          }
        )
    }).catch(error => {
      console.error("Error fetching user data:", error);
    });
  };


  formatDateToDisplay = (dateString) => {
    const options = { year: 'numeric', month: 'long', day: 'numeric' };
    return new Date(dateString).toLocaleDateString(undefined, options);
  }

  render() {

    let data = this.props.data;
    let childinfo = this.state.child;
    
     return (
      <div>
        <section>

          <MDBContainer className="py-5">
            <MDBRow>
              <MDBCol lg="4">
                <MDBCard style={{height: "230px", backgroundColor: "rgba(0, 172, 238, 0.2)",boxShadow: "0 4px 8px 0 rgba(0, 0, 0, 0.2)", border: "none"}} className="mb-4">
                  <MDBCardBody className="text-center" > 
                  <div style={{ marginTop: "20px" }}>
                    <svg xmlns="http://www.w3.org/2000/svg" width="140" height="140" fill="#f2f3f4" className="bi bi-person" viewBox="0 0 16 16">
                      <path d="M8 8a3 3 0 1 0 0-6 3 3 0 0 0 0 6Zm2-3a2 2 0 1 1-4 0 2 2 0 0 1 4 0Zm4 8c0 1-1 1-1 1H3s-1 0-1-1 1-4 6-4 6 3 6 4Zm-1-.004c-.001-.246-.154-.986-.832-1.664C11.516 10.68 10.289 10 8 10c-2.29 0-3.516.68-4.168 1.332-.678.678-.83 1.418-.832 1.664h10Z"/>
                    </svg>
                  </div>
                  </MDBCardBody>
                </MDBCard>
              </MDBCol>

              {/* user profile */}
              <MDBCol lg="8">
                <MDBCard className="mb-4" style={{boxShadow: "0 4px 8px 0 rgba(0, 0, 0, 0.2)", border: "none"}}>
                  <MDBCardBody>
                    <MDBRow>
                      <MDBCol sm="3">
                        <MDBCardText>Full Name</MDBCardText>
                      </MDBCol>
                      <MDBCol sm="9">
                        <MDBCardText className="text-muted">
                        {data[0].name}
                        </MDBCardText>
                      </MDBCol>
                    </MDBRow>
                    <hr />
                    <MDBRow>
                      <MDBCol sm="3">
                        <MDBCardText>Email</MDBCardText>
                      </MDBCol>
                      <MDBCol sm="9">
                        <MDBCardText className="text-muted">
                          {data[0].email}
                        </MDBCardText>
                      </MDBCol>
                    </MDBRow>
                    <hr />
                    <MDBRow>
                      <MDBCol sm="3">
                        <MDBCardText>Phone</MDBCardText>
                      </MDBCol>
                      <MDBCol sm="9">
                        <MDBCardText className="text-muted">
                          {data[0].phone_number}
                        </MDBCardText>
                      </MDBCol>
                    </MDBRow>
                    <hr />
                    <MDBRow>
                      <MDBCol sm="3">
                        <MDBCardText>Date of birth</MDBCardText>
                      </MDBCol>
                      <MDBCol sm="9">
                        <MDBCardText className="text-muted">
                          {this.formatDateToDisplay(data[0].date_of_birth)}
                        </MDBCardText>
                      </MDBCol>
                    </MDBRow>
                  </MDBCardBody>
                </MDBCard>
              </MDBCol>
            </MDBRow>
          </MDBContainer>

        </section>
        <div style={{display: "flex", flexWrap: "wrap", justifyContent: "space-between", maxWidth: "1300px", margin: "auto", paddingBottom: "10px", padidngLeft: "5px" }}>
        {/* Child */}
        {childinfo.length > 0 ?
        childinfo.map((child) => {
          return(
        <div style={{ marginBottom: "20px"}}>
          <div  key={child.id} style={{padding: "5px"}}>
            <div style={{ width: "400px",padding: "1%",margin: "auto",boxShadow: "0 4px 8px 0 rgba(0, 172, 238, 0.5)", border: "none", fontWeight: "500", color: "rgba(0, 172, 238)", marginTop: "-3%", maxWidth: "365px"}}
              className="card">
              <div className="card-body">
                <div style={{textAlign: "center"}}>
                    <svg xmlns="http://www.w3.org/2000/svg" height="2em" viewBox="0 0 320 512">
                      <path fill="rgba(0, 172, 240) " d="M96 64a64 64 0 1 1 128 0A64 64 0 1 1 96 64zm48 320v96c0 17.7-14.3 32-32 32s-32-14.3-32-32V287.8L59.1 321c-9.4 15-29.2 19.4-44.1 10S-4.5 301.9 4.9 287l39.9-63.3C69.7 184 113.2 160 160 160s90.3 24 115.2 63.6L315.1 287c9.4 15 4.9 34.7-10 44.1s-34.7 4.9-44.1-10L240 287.8V480c0 17.7-14.3 32-32 32s-32-14.3-32-32V384H144z"/>
                    </svg>
                </div>
                <hr />
                <div className="card-text">
                  <div style={{display: "flex",justifyContent: "space-between"}}>
                    <div className="text-muted" >Name: </div> <div style={{ width: "50%" }}>{child.name}</div>
                  </div>
                  <hr />

                  <div style={{ display: "flex", justifyContent: "space-between" }}>
                    <div className="text-muted">Surname: </div>{" "}
                    <div style={{ width: "50%" }}>{child.surname}</div>
                  </div>

                  <hr />

                  <div style={{ display: "flex", justifyContent: "space-between" }}>
                    <div className="text-muted">Age: </div> <div style={{ width: "50%" }}>{child.age}</div>
                  </div>

                  <hr/>
                  <div style={{ display: "flex", justifyContent: "space-between" }}>
                    <div className="text-muted" >Favourite food: </div>{" "}
                    <div style={{ width: "50%" }}>{child.favourite_food}</div>
                  </div>

                  <hr/>
                  <div style={{ display: "flex", justifyContent: "space-between" }}>
                    <div className="text-muted">Fovourite game: </div>{" "}
                    <div style={{ width: "50%" }}>{child.favourite_game}</div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        )
       })
        : ""}
      </div>
      </div>
    );
  }
}

export default ProfileView;

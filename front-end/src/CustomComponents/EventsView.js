import React from "react";
import axios from "axios";
import eventimg from "./images/event.png";

class EventsView extends React.Component {
  
  constructor(props) {
    super(props);
    this.state = {
      events: [],
      locations: []
    };
  }

  QSetViewInParent = (obj) => {
    this.props.QIDFromChild(obj);
  };

  async componentDidMount() {
    try {
      const response = await axios.get('/event');
      console.log(response.data);
      const helpevents = response.data;
      this.setState({
        events: response.data
      });
  
      //inizialization empty array
      const allLocations = [];
  
      for (const event of helpevents) {
        try {
          const locationResponse = await axios.get('/location', {
            params: { id: event.location_id }
          });
         // console.log(locationResponse.data);
         //every location stored in that array
          allLocations.push(locationResponse.data);
        } catch (error) {
          console.error('Error fetching location:', error);
        }
      }
  
      //flattens allLocations array of arrays into a single array containing all location data.
      const flattenedLocations = allLocations.flat();
      //console.log(flattenedLocations)
      this.setState({
        locations: flattenedLocations
      });
    } catch (error) {
      console.error('Error fetching events:', error);
    }
  }

  //format date
  formatDate = (dateString) => {
    const date = new Date(dateString);
    const formattedDate = date.toISOString().split('T')[0];
    return formattedDate;
  }

  //format time
  formatTime = (dateString) => {
    const date = new Date(dateString);
    const formattedTime = date.toLocaleTimeString();
    return formattedTime;
  }

  render() {

    //variable for events
    let data = this.state.events;
    //variable for locations
    let dataLocations = this.state.locations;
    
    return (
      <div style={{display: "flex", flexWrap: "wrap", justifyContent: "space-between", maxWidth: "1300px", margin: "auto", paddingBottom: "5px" }}>
     
      {(data.length > 0) && (dataLocations.length > 0 ) ? 
        data.map((d,index) => {
          const location = dataLocations[index]; // Get corresponding location data from dataLocations array
         
          return ( 
            <div style={{ marginBottom: "20px", backgroundColor: "#f2f3f4"}}>
              <div style={{display:"flex"}}>
                <div className="card" style ={{margin: "auto",padding: "1%",width: "400px",flexBasis: "100%", width: "calc(33.33% - 30px)", maxWidth: "400px", margin: "8px",boxShadow: "0 4px 8px 0 rgba(0, 0, 0, 0.2)"}}>
                  <img className="card-img-top" src={eventimg} alt="Card image cap" />
                  <div className="card-body">
                    <h5 className="card-title">{d.title}</h5>
                    <p className="card-text">
                    <div>
                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" className="bi bi-calendar-minus" viewBox="0 0 16 16">
                          <path d="M5.5 9.5A.5.5 0 0 1 6 9h4a.5.5 0 0 1 0 1H6a.5.5 0 0 1-.5-.5z"/>
                          <path d="M3.5 0a.5.5 0 0 1 .5.5V1h8V.5a.5.5 0 0 1 1 0V1h1a2 2 0 0 1 2 2v11a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2V3a2 2 0 0 1 2-2h1V.5a.5.5 0 0 1 .5-.5zM1 4v10a1 1 0 0 0 1 1h12a1 1 0 0 0 1-1V4H1z"/>
                        </svg> {"  "}
                      {this.formatDate(d.date)} 
                    </div>

                    <div>
                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" className="bi bi-alarm" viewBox="0 0 16 16">
                          <path d="M8.5 5.5a.5.5 0 0 0-1 0v3.362l-1.429 2.38a.5.5 0 1 0 .858.515l1.5-2.5A.5.5 0 0 0 8.5 9V5.5z"/>
                          <path d="M6.5 0a.5.5 0 0 0 0 1H7v1.07a7.001 7.001 0 0 0-3.273 12.474l-.602.602a.5.5 0 0 0 .707.708l.746-.746A6.97 6.97 0 0 0 8 16a6.97 6.97 0 0 0 3.422-.892l.746.746a.5.5 0 0 0 .707-.708l-.601-.602A7.001 7.001 0 0 0 9 2.07V1h.5a.5.5 0 0 0 0-1h-3zm1.038 3.018a6.093 6.093 0 0 1 .924 0 6 6 0 1 1-.924 0zM0 3.5c0 .753.333 1.429.86 1.887A8.035 8.035 0 0 1 4.387 1.86 2.5 2.5 0 0 0 0 3.5zM13.5 1c-.753 0-1.429.333-1.887.86a8.035 8.035 0 0 1 3.527 3.527A2.5 2.5 0 0 0 13.5 1z"/>
                        </svg> {"  "} 
                        {this.formatTime(d.time)} 
                    </div>

                    <div> {"  "}
                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" className="bi bi-geo-alt" viewBox="0 0 16 16">
                          <path d="M12.166 8.94c-.524 1.062-1.234 2.12-1.96 3.07A31.493 31.493 0 0 1 8 14.58a31.481 31.481 0 0 1-2.206-2.57c-.726-.95-1.436-2.008-1.96-3.07C3.304 7.867 3 6.862 3 6a5 5 0 0 1 10 0c0 .862-.305 1.867-.834 2.94zM8 16s6-5.686 6-10A6 6 0 0 0 2 6c0 4.314 6 10 6 10z"/>
                          <path d="M8 8a2 2 0 1 1 0-4 2 2 0 0 1 0 4zm0 1a3 3 0 1 0 0-6 3 3 0 0 0 0 6z"/>
                        </svg> {"  "}
                      {location.street} {" "} {location.number} {"-"} {location.city}
                    </div>

                    <div>
                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-geo" viewBox="0 0 16 16">
                          <path fillRule="evenodd" d="M8 1a3 3 0 1 0 0 6 3 3 0 0 0 0-6zM4 4a4 4 0 1 1 4.5 3.969V13.5a.5.5 0 0 1-1 0V7.97A4 4 0 0 1 4 3.999zm2.493 8.574a.5.5 0 0 1-.411.575c-.712.118-1.28.295-1.655.493a1.319 1.319 0 0 0-.37.265.301.301 0 0 0-.057.09V14l.002.008a.147.147 0 0 0 .016.033.617.617 0 0 0 .145.15c.165.13.435.27.813.395.751.25 1.82.414 3.024.414s2.273-.163 3.024-.414c.378-.126.648-.265.813-.395a.619.619 0 0 0 .146-.15.148.148 0 0 0 .015-.033L12 14v-.004a.301.301 0 0 0-.057-.09 1.318 1.318 0 0 0-.37-.264c-.376-.198-.943-.375-1.655-.493a.5.5 0 1 1 .164-.986c.77.127 1.452.328 1.957.594C12.5 13 13 13.4 13 14c0 .426-.26.752-.544.977-.29.228-.68.413-1.116.558-.878.293-2.059.465-3.34.465-1.281 0-2.462-.172-3.34-.465-.436-.145-.826-.33-1.116-.558C3.26 14.752 3 14.426 3 14c0-.599.5-1 .961-1.243.505-.266 1.187-.467 1.957-.594a.5.5 0 0 1 .575.411z"/>
                        </svg> {" "} 
                        {location.description}</div>
                    </p>

                    <hr></hr>

                    <p className="card-text">
                      <small className="text-muted">{d.user_email}</small>
                    </p>

                  </div>

                  <div className="account">
                    <button type="button" onClick={() => {this.QSetViewInParent({page: "joinEvent"})}}>Join</button>
                  </div>

                </div>
              </div>
            </div>
             );
            })
          :  <small style={{fontWeight: "500"}}>There are currently no events available!</small>} 
      </div>
    );
  }
}

export default EventsView;

import React from "react";
import "./buttonStyles.css";
import axios from "axios";

class AddEventView extends React.Component {
  componentDidMount() {
    document.body.style.backgroundImage = "none";
    document.body.style.backgroundColor = "#f2f3f4";
  }

  constructor(props) {
    super(props);
    this.state = {
      event: {}
    };
  }

  QSetViewInParent = (obj) => {
    this.props.QIDFromChild(obj);
  };

  QGetTextFromField = (e) => {
    this.setState((prevState) => ({
      event: { ...prevState.event, [e.target.name]: e.target.value }
    }));
  };

  QPostEvent = async ()=>{

    const time = this.state.event.time; 

    const formatted_time = `2023-07-19 ${time}:00`;

    axios.post('/event',{
      title:this.state.event.title,
      date:this.state.event.date,
      time:formatted_time,
      description1:this.state.event.description1,
      street:this.state.event.street,
      number:this.state.event.number,
      city:this.state.event.city,
      description:this.state.event.description
    },{
      withCredentials: true,
    })
    .then(response=>{
      console.log("Sent to server...")
      this.props.QIDFromChild({page: "events"})
    })
    .catch(err=>{
      console.log(err)
      alert(err.response.data.message)
    })
   
  };

  render() {
    return (
      <div style={{marginTop: "10px", maxWidth: "1100px", margin: "auto"}}>
        <div
          style={{
            marginLeft: "auto",
            marginRight: "auto",
            //marginTop: "30px",
            marginBottom: "10px",
            textAlign: "center",
            fontStyle: "normal",
            fontFamily: "Roboto",
            fontSize: "30px"
          }}
        >
          Create Event
        </div>
        <div
          className="card"
          style={{ margin: "auto", minWidth: "350px", width: "50%" }}
        >
          <div className="mb-3" style={{ margin: "10px" }}>
            <label className="form-label">Title</label>
            <input name="title" type="text" className="form-control" onChange={(e) => this.QGetTextFromField(e)}/>
          </div>

          <div style={{display: "flex", width: "100%"}}>
          <div className="mb-3" style={{ margin: "10px", display: "flex", flexDirection: "column", minWidth: "45%" }}>
            <label className="form-label">Date</label>
            <input
              name="date"
              type="date"
              className="form-control"
              placeholder="Date"
              onChange={(e) => this.QGetTextFromField(e)}
            />
          </div>

          <div className="mb-3" style={{ margin: "10px", display: "flex", flexDirection: "column", minWidth: "45%" }}>
            <label className="form-label">Time</label>
            <input
              name="time"
              type="time"
              className="form-control"
              placeholder="Date"
              onChange={(e) => this.QGetTextFromField(e)}
            />
          </div>

          </div>
          <div className="mb-3" style={{ margin: "10px" }}>
            <label className="form-label">Description</label>
            <textarea name="description1" className="form-control" rows="3" onChange={(e) => this.QGetTextFromField(e)}></textarea>
          </div>

          <div style={{display: "flex", width: "93%"}}>
              <div className="mb-3" style={{ margin: "10px", display: "flex", flexDirection: "column", minWidth: "30%" }}>
                <label className="form-label">Street</label>
                <input name= "street" type="text" className="form-control" rows="3" onChange={(e) => this.QGetTextFromField(e)}></input>
              </div>
              <div className="mb-3" style={{ margin: "10px",display: "flex", flexDirection: "column", minWidth: "30%" }}>
                <label className="form-label">Number</label>
                <input name="number" type="text"  className="form-control" rows="3" onChange={(e) => this.QGetTextFromField(e)}></input>
              </div>
              <div className="mb-3" style={{ margin: "10px", display: "flex", flexDirection: "column", minWidth: "30%" }}>
                <label className="form-label">City</label>
                <input name="city" type="text"  className="form-control" rows="3" onChange={(e) => this.QGetTextFromField(e)}></input>
              </div>
          </div>

          <div className="mb-3" style={{ margin: "10px" }}>
            <label className="form-label">Meeting point</label>
            <input name="description" type="text" className="form-control" rows="3" onChange={(e) => this.QGetTextFromField(e)}></input>
          </div>

          <div className="account">
            <button onClick={() => this.QPostEvent()} type="button">
              Create
            </button>
          </div>
        </div>
      </div>
    );
  }
}

export default AddEventView;

import React from "react";
import "./buttonStyles.css";
import axios from "axios";

class SignView extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      user: {
        type: "signup"
      }
    };
  }

  //color for the view
  componentDidMount() {
    document.body.style.backgroundImage = "none";
    document.body.style.backgroundColor = "#f2f3f4";
  }

  //get the input from the form
  QGetTextFromField = (e) => {
    this.setState((prevState) => ({
      user: { ...prevState.user, [e.target.name]: e.target.value }
    }));
  };

  QPostSignup = () => {
    axios
      .post("/users/register", {
        email: this.state.user.email,
        name: this.state.user.name,
        phone_number: this.state.user.phone_number,
        date_of_birth: this.state.user.date,
        password: this.state.user.password
      })
      .then((response) => {

        //check the response
        if(response.data === "A field is missing!"){
          alert("All fields are required!")
          return;
        }
        else{
        console.log("Sent to server...");
        }
      })
      .catch((err) => {
        console.log(err);
      });

      this.QSetViewInParent({ page: "login" });
  };

  //change view
  QSetViewInParent = (obj) => {
    this.props.QIDFromChild(obj);
  };

  render() {
    return (
      <div>
        <div
          style={{
            maxWidth: "400px",
            marginLeft: "auto",
            marginRight: "auto",
            marginTop: "40px",
            marginBottom: "10px",
            textAlign: "center",
            fontStyle: "normal",
            fontFamily: "Roboto",
            fontSize: "30px"
          }}
        >
          Create Account
        </div>
        <div
          className="card"
          style={{
            maxWidth: "350px",
            marginLeft: "auto",
            marginRight: "auto",
            marginTop: "30px",
            marginBottom: "10px"
          }}
        >
          <form style={{ margin: "20px" }}>
            <div className="mb-3">
              <label className="form-label">Name</label>
              <input
                onChange={(e) => this.QGetTextFromField(e)}
                name="name"
                type="text"
                className="form-control"
              />
              <label className="form-label">Email</label>
              <input
                onChange={(e) => this.QGetTextFromField(e)}
                name="email"
                type="email"
                className="form-control"
                aria-describedby="emailHelp"
              />
              <label className="form-label">Phone number</label>
              <input
                onChange={(e) => this.QGetTextFromField(e)}
                name="phone_number"
                type="tel"
                placeholder="+386 XX XXX XXX"
                className="form-control"
              />
            </div>
            <div className="mb-3">
              <label className="form-label">Date of birth</label>
              <input
                onChange={(e) => this.QGetTextFromField(e)}
                name="date"
                type="date"
                className="form-control"
              />
            </div>
            <div className="mb-3">
              <label className="form-label">Password</label>
              <input
                onChange={(e) => this.QGetTextFromField(e)}
                name="password"
                type="password"
                className="form-control"
              />
            </div>
            <div className="account">
              <button onClick={() => this.QPostSignup()}>Create Account</button>
            </div>

            <div style={{ textAlign: "center" }}>
              <a onClick={() => this.QSetViewInParent({ page: "login" })}>
                <small>Already Have an Account?</small>
              </a>
            </div>

          </form>
        </div>
      </div>
    );
  }
}

export default SignView;

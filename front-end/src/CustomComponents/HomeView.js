import React from "react";
import Logo2 from "./images/logo.png";

class HomeView extends React.Component {

  //color for the view
  componentDidMount() {
    document.body.style.backgroundImage = "none";
    document.body.style.backgroundColor = "#f2f3f4";
  };


  render() {
    return (
      <div style={{ margin: "auto" }}>
        <div
          style={{
            padding: "20px"
          }}
        >
          <h5
            style={{
              padding: "-10px",
              marginTop: "3px",
              fontStyle: "normal",
              textAlign: "center",
              fontFamily: "Roboto",
              fontSize: "calc(20px + 5.5vw)",
              fontWeight: "400",
              color: "#00ACEE",
              letterSpacing: "3px",
              textShadow: "2px 2px 4px rgba(0, 0, 0, 0.2)",
            }}
          >
            NANNY ME
          </h5>
        </div>
        <div
          style={{
            maxWidth: "500px",
            maxHeight: "600px",
            margin: "auto",
            textAlign: "center"
          }}
        >
          <img
            className="center"
            src={Logo2}
            alt="React Logo"
            height="auto"
            width="100%"
            id="image-section"
          />
        </div>
        <div
          style={{ textAlign: "center",fontStyle: "italic", fontFamily: "Roboto",color: "#00ACEE",fontWeight: "400",fontSize: "calc(15px + 1.8vw)" }}>
          "Your child is in caring hands."
        </div>
      </div>
    );
  }
}

export default HomeView;

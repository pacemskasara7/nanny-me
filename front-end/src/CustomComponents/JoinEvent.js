import React from "react";
import "./buttonStyles.css";
import eventJoin from "./images/familyJoinevent.png";


class JoinEvent extends React.Component {

  //colors
  componentDidMount() {
    document.body.style.backgroundImage = "none";
    document.body.style.backgroundColor = "#f2f3f4";
  }

  //chenge the view
  QSetViewInParent = (obj) => {
    this.props.QIDFromChild(obj);
  };

  render() {
    return (
      <div style={{textAlign: "center"}}>
          <h5
            style={{
              padding: "-10px",
              marginTop: "3px",
              fontStyle: "normal",
              fontFamily: "Roboto",
              fontWeight: "400",
              fontSize: "calc(12px + 3.5vw)",
              letterSpacing: "3px"
            }}
          >
            Thank you
          </h5>
          <div style={{
              padding: "-10px",
              marginTop: "3px",
              fontStyle: "normal",
              fontFamily: "Roboto",
              fontSize: "calc(11px + 2.2vw)",
              fontWeight: "400",
              letterSpacing: "3px"
            }}>for expressing interest in our family event! </div>
              <br></br>
            <img style={{width: "50%", maxWidth: "700px"}} src={eventJoin} alt="Card image cap" />
            <br></br>
            <br></br>
            <br></br>
          <div style={{
              padding: "-10px",
              marginTop: "3px",
              fontStyle: "italic",
              fontFamily: "Roboto",
              fontWeight: "400",
              letterSpacing: "3px",
              fontSize: "calc(8px + 1.0vw)"
              }}>
                We are thrilled to welcome you and your loved ones to a day filled with joy, laughter, and unforgettable memories.See you soon!
          </div>
      </div>
    );
  }
}

export default JoinEvent;

import React from "react";
import "./buttonStyles.css";
import axios from "axios";

class LoginView extends React.Component {

  //constructor
  constructor(props) {
    super(props);
    this.state = {
      user: []
    };
  };

  componentDidMount() {
    //setting the colors
    document.body.style.backgroundImage = "none";
    document.body.style.backgroundColor = "#f2f3f4";
  };

  //get the input from the form
  QGetTextFromField = (e) => {
    this.setState((prevState) => ({
      user: { ...prevState.user, [e.target.name]: e.target.value }
    }));
  };

  //change the view
  QSetViewInParent = (obj) => {
    this.props.QIDFromChild(obj);
  };

  //posting the information for logging
  QPostLogin = () => {
    axios
      .post(
        "/users/login",
        {
          email: this.state.user.email,
          password: this.state.user.password
        },
        { withCredentials:true}
      )
      .then((response) => {
        console.log(response.data)
        console.log("Sent to server...");
        console.log(this.state.user)

        //check the response from the back-end
        if(response.data === "Incorrect pass"){
          alert("Incorrect Password!")
          this.QSetViewInParent({ page: "login"})
        }
        else if(response.data === "Not registered"){
          alert("User not registered!")
          this.QSetViewInParent({page: "login"})
        }
        else if(response.data === "Please enter username and password"){
          alert("Please enter Username and Password!")
          this.QSetViewInParent({page: "login"})
        }
        else {
          this.QSendUser2Parent(response.data[0]);
          console.log(response.data[0])
        }
      })
      .catch((err) => {
        console.log(err);
      });
  };

  //setting the information for the user
  QSendUser2Parent = (obj) => {
    this.props.QUserFromChild(obj);
  };

  render() {
    return (
      <div style={{marginTop: "-10px"}}>
        <div
          style={{
            maxWidth: "400px",
            marginLeft: "auto",
            marginRight: "auto",
            marginTop: "40px",
            marginBottom: "10px",
            textAlign: "center",
            fontStyle: "normal",
            fontFamily: "Roboto",
            fontSize: "30px"
          }}
        >
          Login
        </div>
        <div
          style={{
            maxWidth: "350px",
            marginLeft: "auto",
            marginRight: "auto",
            marginBottom: "2px",
            textAlign: "left",
            fontStyle: "italic",
            fontFamily: "Roboto",
            fontSize: "13px"
          }}
        >
          {/* Enter your details below to continue */}
        </div>
        <div
          className="card"
          style={{
            maxWidth: "350px",
            marginLeft: "auto",
            marginRight: "auto",
            marginTop: "40px",
            marginBottom: "10px"
          }}
        >
          <form style={{ margin: "20px" }}>
            <div className="mb-3">
              <label className="form-label">Email</label>
              <input
                onChange={(e) => this.QGetTextFromField(e)}
                name="email"
                type="email"
                className="form-control"
                id="exampleInputEmail1"
              />
            </div>
            <div className="mb-3">
              <label className="form-label">Password</label>
              <input
                onChange={(e) => this.QGetTextFromField(e)}
                name="password"
                type="password"
                className="form-control"
                id="exampleInputPassword1"
              />
            </div>
            <div className="account">
              <button
                onClick={() => {this.QPostLogin();this.QSetViewInParent({page: "profile"})}}
                type="button"
              >
                Login
              </button>
              <a onClick={() => this.QSetViewInParent({ page: "signup" })}>
                {" "}
                <small>Don't have an account?</small>{" "}
              </a>
            </div>
          </form>
        </div>
      </div>
    );
  }
}


export default LoginView;

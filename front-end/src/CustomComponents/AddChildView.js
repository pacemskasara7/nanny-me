import React from "react";
import "./buttonStyles.css";
import axios from "axios";

class AddChildView extends React.Component {
    
  constructor(props) {
    super(props);
    this.state = {
      child: {}
    };
  }

  componentDidMount() {
    //setting the colors
    document.body.style.backgroundImage = "none";
    document.body.style.backgroundColor = "#f2f3f4";
  }

  //function to get the input and store in child
  QGetTextFromField = (e) => {
    this.setState((prevState) => ({
      child: { ...prevState.child, [e.target.name]: e.target.value }
    }));
  };

  //post information for the child
  QPostChild=(e)=>{
    axios.post('/child',{
      name:this.state.child.name,
      surname:this.state.child.surname,
      age:this.state.child.age,
      favourite_food:this.state.child.food,
      favourite_game:this.state.child.game
    },{
      withCredentials: true,
    })
    .then(response=>{
      console.log(response.data)
      console.log("Sent to server...")
      this.props.QIDFromChild({page: "profile", childinfo: response.data})
  
    })
    .catch(err=>{
      console.log(err)
      alert(err.response.data.message)
    })
  };

  
  QSetViewInParent = (obj) => {
    this.props.QIDFromChild(obj);
  };

  render() {
    return (
      <div>
        <div
          style={{
            maxWidth: "400px",
            marginLeft: "auto",
            marginRight: "auto",
            marginTop: "40px",
            marginBottom: "10px",
            textAlign: "center",
            fontStyle: "normal",
            fontFamily: "Roboto",
            fontSize: "30px"
          }}
        >
          Create Child Profile
        </div>
        <div
          className="card"
          style={{
            maxWidth: "350px",
            marginLeft: "auto",
            marginRight: "auto",
            marginTop: "30px",
            marginBottom: "10px"
          }}
        >
          <div style={{ margin: "20px" }}>
            <div className="mb-3">
              <label className="form-label">Name</label>
              <input
                onChange={(e) => this.QGetTextFromField(e)}
                name="name"
                type="text"
                className="form-control"
              />
              <label className="form-label">Surname</label>
              <input
                onChange={(e) => this.QGetTextFromField(e)}
                name="surname"
                type="text"
                className="form-control"
              />
              <label className="form-label">Age</label>
              <input
                onChange={(e) => this.QGetTextFromField(e)}
                name="age"
                type="number"
                min="1"
                className="form-control"
              />
            </div>
            <div className="mb-3">
              <label className="form-label">Fovourite food</label>
              <input
                onChange={(e) => this.QGetTextFromField(e)}
                name="food"
                type="text"
                className="form-control"
              />
            </div>
            <div className="mb-3">
              <label className="form-label">Favourite game</label>
              <input
                onChange={(e) => this.QGetTextFromField(e)}
                name="game"
                type="text"
                className="form-control"
              />
            </div>
            <div className="account">
              <button onClick={() => this.QPostChild()}>Create</button>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default AddChildView;

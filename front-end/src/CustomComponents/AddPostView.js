import React from "react";
import "./buttonStyles.css";
import axios from "axios";

class AddPostView extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      posts: {}
    };
  }

  //colors
  componentDidMount() {
    document.body.style.backgroundImage = "none";
    document.body.style.backgroundColor = "#f2f3f4";
  }

  //get input ans save in state-posts
  QGetTextFromField = (e) => {
    this.setState((prevState) => ({
      posts: { ...prevState.posts, [e.target.name]: e.target.value }
    }));
  };

  QPostPost = async () => {

    //variables for formated date and time
    const time_from = this.state.posts.time_from; 
    const time_to = this.state.posts.time_to; 

    const formatted_time_from = `2023-07-19 ${time_from}:00`;
    const formatted_time_to = `2023-07-19 ${time_to}:00`;

    try {
      const response = await axios.post(
        '/post',
        {
          name_child: this.state.posts.name_child,
          date: this.state.posts.date,
          time_from: formatted_time_from,
          time_to: formatted_time_to,
          description: this.state.posts.description,
        },
        {
          withCredentials: true,
        }
      );
      console.log("Sent to server...");
      console.log(response);
      this.props.QIDFromChild({ page: "posts" });
    } catch (err) {
      //console.log(err);
      alert(err.response.data.message)
    }
  };
  
  //change the view
  QSetViewInParent = (obj) => {
    this.props.QIDFromChild(obj);
  };

  render() {
    return (
      <div>
        <div
          style={{
            marginLeft: "auto",
            marginRight: "auto",
            marginTop: "40px",
            marginBottom: "10px",
            textAlign: "center",
            fontStyle: "normal",
            fontFamily: "Roboto",
            fontSize: "30px"
          }}
        >
          Create Post
        </div>
        <div
          className="card"
          style={{ margin: "auto", minWidth: "350px", width: "50%" }}
        >
          <div className="mb-3" style={{ margin: "10px" }}>
            <label className="form-label">Name of the child</label>
            <input name="name_child" type="text" className="form-control" placeholder="Name..."  onChange={(e) => this.QGetTextFromField(e)}/>
          </div>
          <div className="mb-3" style={{ margin: "10px" }}>
            <label className="form-label">Date</label>
            <input name="date" type="date" className="form-control" onChange={(e) => this.QGetTextFromField(e)} />
          </div>
          <div className="mb-3" style={{ margin: "10px" }}>
            <label className="form-label">Time - From</label>
            <input name="time_from" type="time" className="form-control" onChange={(e) => this.QGetTextFromField(e)} />
          </div>
          <div className="mb-3" style={{ margin: "10px" }}>
            <label className="form-label">Time - To</label>
            <input name= "time_to" type="time" className="form-control" onChange={(e) => this.QGetTextFromField(e)}/>
          </div>
          <div className="mb-3" style={{ margin: "10px" }}>
            <label className="form-label">Description</label>
            <textarea name = "description" type= "text" className="form-control" onChange={(e) => this.QGetTextFromField(e)}/>
          </div>
          <div className="account">
            <button onClick={() => this.QPostPost()}>Send</button>
          </div>
        </div>
      </div>
    );
  }
}

export default AddPostView;

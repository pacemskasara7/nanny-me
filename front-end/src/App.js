import React from "react";

//Import CustomComponents
import HomeView from "./CustomComponents/HomeView";
import LoginView from "./CustomComponents/LoginView";
import SignView from "./CustomComponents/SignView";
import AboutView from "./CustomComponents/AboutViiew";
import AddPostView from "./CustomComponents/AddPostView";
import PostView from "./CustomComponents/PostView";
import EventsView from "./CustomComponents/EventsView";
import AddEventView from "./CustomComponents/AddEventView";
import ProfileView from "./CustomComponents/ProfileView";
import AddChildView from "./CustomComponents/AddChildView";
import JoinEvent from "./CustomComponents/JoinEvent";
import Chat from "./CustomComponents/ChatView2";
import axios from "axios";


class App extends React.Component {
  //The constructor of our app.
  constructor(props) {
    super(props);
    //state is where our "global" variable will be store
    this.state = { 
      CurrentPage: "home", 
      child: {}, 
      userStatus:{logged:false}, 
      emailReceive: "" , 
      messageReceive: "",
      email2: "", 
      prevPage: ""
    };
  }


  QSetView = (obj) => {
    this.setState((prevState) => ({
      
      CurrentPage: obj.page,
      prevPage: prevState.CurrentPage,
      event: obj.id || 0,
      posts: obj.id || 0,
      child: obj.childinfo || {},
      emailReceive: obj.emailRec || "",
      messageReceive: obj.textMessage || "",
      email2: obj.emailSend || "",
      
    }));
  };

  handleLogout = () => {

    if (!this.state.userStatus.logged) {
      console.log("User is not logged in. No need to logout.");
      <div style={{paddingLeft: "10px", backgroundColor: "#EDB6B5", display: "flex", flexDirection: "row"}} > 
      <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="currentColor" className="bi bi-exclamation-triangle" viewBox="0 0 16 16">
         <path d="M7.938 2.016A.13.13 0 0 1 8.002 2a.13.13 0 0 1 .063.016.146.146 0 0 1 .054.057l6.857 11.667c.036.06.035.124.002.183a.163.163 0 0 1-.054.06.116.116 0 0 1-.066.017H1.146a.115.115 0 0 1-.066-.017.163.163 0 0 1-.054-.06.176.176 0 0 1 .002-.183L7.884 2.073a.147.147 0 0 1 .054-.057zm1.044-.45a1.13 1.13 0 0 0-1.96 0L.165 13.233c-.457.778.091 1.767.98 1.767h13.713c.889 0 1.438-.99.98-1.767L8.982 1.566z"/>
         <path d="M7.002 12a1 1 0 1 1 2 0 1 1 0 0 1-2 0zM7.1 5.995a.905.905 0 1 1 1.8 0l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 5.995z"/>
      </svg>
      <h5>This page requires login to access. Please login first.</h5> 
     </div>
      return;
    }

    // Clear cookies or session data here
    document.cookie = "connect.sid=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;";
    
    this.QSetView({ page: "login" });
    this.setState({
      userStatus: { logged: false, user: [] }
    })
  };

  componentDidMount(){
    axios.get('/users/login')
    .then(response=>{
      console.log(response)
    })
   };

   QSetUser = (obj) => {
    this.setState({
      userStatus: { logged: true, user: [obj] }
    },
    //calback function to see immediatelly the change of the state
    () => {
      console.log(this.state.userStatus); // Log the state inside the callback
    }
    );
  };

  QGetView = (state) => {
    let page = state.CurrentPage;

    switch (page) {

      case "about":
        return <AboutView QIDFromChild={this.QSetView} />;

      case "home":
        return <HomeView QIDFromChild={this.QSetView} />;

      case "posts":
        return state.userStatus.logged ? 
        (
          <PostView data = {this.state.userStatus.user} QIDFromChild={this.QSetView}  />
        ) : (
          <div style={{paddingLeft: "10px", backgroundColor: "#EDB6B5", display: "flex", flexDirection: "row"}}> 
          <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="currentColor" className="bi bi-exclamation-triangle" viewBox="0 0 16 16">
              <path d="M7.938 2.016A.13.13 0 0 1 8.002 2a.13.13 0 0 1 .063.016.146.146 0 0 1 .054.057l6.857 11.667c.036.06.035.124.002.183a.163.163 0 0 1-.054.06.116.116 0 0 1-.066.017H1.146a.115.115 0 0 1-.066-.017.163.163 0 0 1-.054-.06.176.176 0 0 1 .002-.183L7.884 2.073a.147.147 0 0 1 .054-.057zm1.044-.45a1.13 1.13 0 0 0-1.96 0L.165 13.233c-.457.778.091 1.767.98 1.767h13.713c.889 0 1.438-.99.98-1.767L8.982 1.566z"/>
              <path d="M7.002 12a1 1 0 1 1 2 0 1 1 0 0 1-2 0zM7.1 5.995a.905.905 0 1 1 1.8 0l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 5.995z"/>
          </svg>
        <h5>This page requires login to access. Please login first.</h5> </div>
        );

      case "addpost":
        return state.userStatus.logged ? (
          <AddPostView QIDFromChild={this.QSetView} />
        ) : (
          <div style={{paddingLeft: "10px", backgroundColor: "#EDB6B5", display: "flex", flexDirection: "row"}} > 
           <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="currentColor" className="bi bi-exclamation-triangle" viewBox="0 0 16 16">
              <path d="M7.938 2.016A.13.13 0 0 1 8.002 2a.13.13 0 0 1 .063.016.146.146 0 0 1 .054.057l6.857 11.667c.036.06.035.124.002.183a.163.163 0 0 1-.054.06.116.116 0 0 1-.066.017H1.146a.115.115 0 0 1-.066-.017.163.163 0 0 1-.054-.06.176.176 0 0 1 .002-.183L7.884 2.073a.147.147 0 0 1 .054-.057zm1.044-.45a1.13 1.13 0 0 0-1.96 0L.165 13.233c-.457.778.091 1.767.98 1.767h13.713c.889 0 1.438-.99.98-1.767L8.982 1.566z"/>
              <path d="M7.002 12a1 1 0 1 1 2 0 1 1 0 0 1-2 0zM7.1 5.995a.905.905 0 1 1 1.8 0l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 5.995z"/>
           </svg>
           <h5>This page requires login to access. Please login first.</h5> 
          </div>
        );
        
      case "signup":
        return <SignView QIDFromChild={this.QSetView} />;

      case "login":
        return <LoginView  QIDFromChild={this.QSetView} QUserFromChild={this.QSetUser} />;

      case "events":
        return state.userStatus.logged ? (
          <EventsView QIDFromChild={this.QSetView} />
        ) : (
          <div style={{paddingLeft: "10px", backgroundColor: "#EDB6B5", display: "flex", flexDirection: "row"}} >
             <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="currentColor" className="bi bi-exclamation-triangle" viewBox="0 0 16 16">
               <path d="M7.938 2.016A.13.13 0 0 1 8.002 2a.13.13 0 0 1 .063.016.146.146 0 0 1 .054.057l6.857 11.667c.036.06.035.124.002.183a.163.163 0 0 1-.054.06.116.116 0 0 1-.066.017H1.146a.115.115 0 0 1-.066-.017.163.163 0 0 1-.054-.06.176.176 0 0 1 .002-.183L7.884 2.073a.147.147 0 0 1 .054-.057zm1.044-.45a1.13 1.13 0 0 0-1.96 0L.165 13.233c-.457.778.091 1.767.98 1.767h13.713c.889 0 1.438-.99.98-1.767L8.982 1.566z"/>
               <path d="M7.002 12a1 1 0 1 1 2 0 1 1 0 0 1-2 0zM7.1 5.995a.905.905 0 1 1 1.8 0l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 5.995z"/>
            </svg>
        <h5>This page requires login to access. Please login first.</h5> </div>
        );

      case "addevents":
        return state.userStatus.logged ? (
          <AddEventView QIDFromChild={this.QSetView} />
        ) : (
          <div style={{paddingLeft: "10px", backgroundColor: "#EDB6B5", display: "flex", flexDirection: "row"}} > 
            <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="currentColor" className="bi bi-exclamation-triangle" viewBox="0 0 16 16">
              <path d="M7.938 2.016A.13.13 0 0 1 8.002 2a.13.13 0 0 1 .063.016.146.146 0 0 1 .054.057l6.857 11.667c.036.06.035.124.002.183a.163.163 0 0 1-.054.06.116.116 0 0 1-.066.017H1.146a.115.115 0 0 1-.066-.017.163.163 0 0 1-.054-.06.176.176 0 0 1 .002-.183L7.884 2.073a.147.147 0 0 1 .054-.057zm1.044-.45a1.13 1.13 0 0 0-1.96 0L.165 13.233c-.457.778.091 1.767.98 1.767h13.713c.889 0 1.438-.99.98-1.767L8.982 1.566z"/>
              <path d="M7.002 12a1 1 0 1 1 2 0 1 1 0 0 1-2 0zM7.1 5.995a.905.905 0 1 1 1.8 0l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 5.995z"/>
            </svg>
          <h5>This page requires login to access. Please login first.</h5> 
        </div>
        );

      case "profile":
        return state.userStatus.logged ? (
          <ProfileView childinfo={this.state.child} data = {this.state.userStatus.user} QIDFromChild={this.QSetView} />
        ) : (
          <div style={{paddingLeft: "10px", backgroundColor: "#EDB6B5", display: "flex", flexDirection: "row"}} > 
            <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="currentColor" className="bi bi-exclamation-triangle" viewBox="0 0 16 16">
              <path d="M7.938 2.016A.13.13 0 0 1 8.002 2a.13.13 0 0 1 .063.016.146.146 0 0 1 .054.057l6.857 11.667c.036.06.035.124.002.183a.163.163 0 0 1-.054.06.116.116 0 0 1-.066.017H1.146a.115.115 0 0 1-.066-.017.163.163 0 0 1-.054-.06.176.176 0 0 1 .002-.183L7.884 2.073a.147.147 0 0 1 .054-.057zm1.044-.45a1.13 1.13 0 0 0-1.96 0L.165 13.233c-.457.778.091 1.767.98 1.767h13.713c.889 0 1.438-.99.98-1.767L8.982 1.566z"/>
              <path d="M7.002 12a1 1 0 1 1 2 0 1 1 0 0 1-2 0zM7.1 5.995a.905.905 0 1 1 1.8 0l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 5.995z"/>
            </svg>
          <h5>This page requires login to access. Please login first.</h5> 
        </div>
        );

      case "addChild":
        return state.userStatus.logged ? (
          <AddChildView QIDFromChild={this.QSetView} />
        ) : (
          <div style={{paddingLeft: "10px", backgroundColor: "#EDB6B5", display: "flex", flexDirection: "row"}} > 
            <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="currentColor" className="bi bi-exclamation-triangle" viewBox="0 0 16 16">
              <path d="M7.938 2.016A.13.13 0 0 1 8.002 2a.13.13 0 0 1 .063.016.146.146 0 0 1 .054.057l6.857 11.667c.036.06.035.124.002.183a.163.163 0 0 1-.054.06.116.116 0 0 1-.066.017H1.146a.115.115 0 0 1-.066-.017.163.163 0 0 1-.054-.06.176.176 0 0 1 .002-.183L7.884 2.073a.147.147 0 0 1 .054-.057zm1.044-.45a1.13 1.13 0 0 0-1.96 0L.165 13.233c-.457.778.091 1.767.98 1.767h13.713c.889 0 1.438-.99.98-1.767L8.982 1.566z"/>
              <path d="M7.002 12a1 1 0 1 1 2 0 1 1 0 0 1-2 0zM7.1 5.995a.905.905 0 1 1 1.8 0l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 5.995z"/>
            </svg>
            <h5>This page requires login to access. Please login first.</h5> 
          </div>
        );

      case "joinEvent":
        return state.userStatus.logged ? ( <JoinEvent QIDFromChild={this.QSetView}/>
        ) : (
          <div style={{paddingLeft: "10px", backgroundColor: "#EDB6B5", display: "flex", flexDirection: "row"}} > 
            <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="currentColor" className="bi bi-exclamation-triangle" viewBox="0 0 16 16">
              <path d="M7.938 2.016A.13.13 0 0 1 8.002 2a.13.13 0 0 1 .063.016.146.146 0 0 1 .054.057l6.857 11.667c.036.06.035.124.002.183a.163.163 0 0 1-.054.06.116.116 0 0 1-.066.017H1.146a.115.115 0 0 1-.066-.017.163.163 0 0 1-.054-.06.176.176 0 0 1 .002-.183L7.884 2.073a.147.147 0 0 1 .054-.057zm1.044-.45a1.13 1.13 0 0 0-1.96 0L.165 13.233c-.457.778.091 1.767.98 1.767h13.713c.889 0 1.438-.99.98-1.767L8.982 1.566z"/>
              <path d="M7.002 12a1 1 0 1 1 2 0 1 1 0 0 1-2 0zM7.1 5.995a.905.905 0 1 1 1.8 0l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 5.995z"/>
            </svg>
            <h5>This page requires login to access. Please login first.</h5> 
          </div>
        );

      case "chat2":
        return state.userStatus.logged ? (<Chat userEmailReceive= {this.state.emailReceive} data = {this.state.userStatus.user} dataMesage={this.state.messageReceive} paramsEmail ={this.state.email2}  prevPage={this.state.prevPage} QIDFromChild={this.QSetView}/>
        ) : (
        <div style={{paddingLeft: "10px", backgroundColor: "#EDB6B5", display: "flex", flexDirection: "row"}} > 
          <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="currentColor" className="bi bi-exclamation-triangle" viewBox="0 0 16 16">
            <path d="M7.938 2.016A.13.13 0 0 1 8.002 2a.13.13 0 0 1 .063.016.146.146 0 0 1 .054.057l6.857 11.667c.036.06.035.124.002.183a.163.163 0 0 1-.054.06.116.116 0 0 1-.066.017H1.146a.115.115 0 0 1-.066-.017.163.163 0 0 1-.054-.06.176.176 0 0 1 .002-.183L7.884 2.073a.147.147 0 0 1 .054-.057zm1.044-.45a1.13 1.13 0 0 0-1.96 0L.165 13.233c-.457.778.091 1.767.98 1.767h13.713c.889 0 1.438-.99.98-1.767L8.982 1.566z"/>
            <path d="M7.002 12a1 1 0 1 1 2 0 1 1 0 0 1-2 0zM7.1 5.995a.905.905 0 1 1 1.8 0l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 5.995z"/>
           </svg>
           <h5>This page requires login to access. Please login first.</h5> 
        </div>
      );
      default:
        return <HomeView />;
    }
  };

  render() {
    
    return (
      <div>
        <div id="APP">
          <div id="menu">
            <nav id="navbar" className="navbar navbar-expand-lg navbar" style={{backgroundColor: "#f2f3f4"}}>
              <div className="container-fluid">
                <a onClick={() => this.QSetView({ page: "home" })} className="navbar-brand" href="#">
                  Home
                </a>
                <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                  <span className="navbar-toggler-icon"></span>
                </button>

                <div className="collapse navbar-collapse com-sm" id="navbarSupportedContent">
                  <div className="navbar-nav me-auto mb-2 mb-lg-0">
                    <li className="nav-item">
                      <a onClick={() => this.QSetView({ page: "about" })} className="nav-link " href="#">
                        About
                      </a>
                    </li>

                    {/* Profile */}
                    <div className="nav-item ">
                      <div className="dropdown">
                        <div className="nav-item " data-bs-toggle="dropdown" aria-expanded="false">
                          <a className="nav-link dropdown-toggle" href="#">
                            Profile
                          </a>
                        </div>

                        <ul className="dropdown-menu text-small shadow">
                          <li>
                            <a onClick={() => this.QSetView({ page: "profile" })} className="dropdown-item" href="#">
                              Show profile
                            </a>
                          </li>
                          <li>
                            <a onClick={() =>this.QSetView({ page: "addChild" })} className="dropdown-item" href="#">
                              Add child profile
                            </a>
                          </li>
                          <li>
                            <a className="dropdown-item" href="#" onClick={() => this.handleLogout()}>
                              Sign out
                            </a>
                          </li>
                        </ul>

                      </div>
                    </div>

                    {/* Posts */}
                    <div className="nav-item ">
                      <div className="dropdown">
                        <div className="nav-item " data-bs-toggle="dropdown" aria-expanded="false" >
                          <a className="nav-link dropdown-toggle" href="#">
                            Posts
                          </a>
                        </div>

                        <ul className="dropdown-menu text-small shadow">
                          <li>
                            <a onClick={() => this.QSetView({ page: "posts" })} className="dropdown-item" href="#">
                              Show posts
                            </a>
                          </li>

                          <li>
                            <a onClick={() => this.QSetView({ page: "addpost" })} className="dropdown-item" href="#" >
                              Add post
                            </a>
                          </li>
                        </ul>
                      </div>
                    </div>

                    {/* Events */}
                    <div className="nav-item ">
                      <div className="dropdown">
                        <div className="nav-item " data-bs-toggle="dropdown" aria-expanded="false">
                          <a className="nav-link dropdown-toggle" href="#">
                            Events
                          </a>
                        </div>

                        <ul className="dropdown-menu text-small shadow">
                          <li>
                            <a onClick={() => this.QSetView({ page: "events" })} className="dropdown-item" href="#">
                              Show events
                            </a>
                          </li>

                          <li>
                            <a onClick={() =>this.QSetView({ page: "addevents" })} className="dropdown-item" href="#">
                              Create event
                            </a>
                          </li>
                        </ul>
                      </div>
                      
                    </div>
                    <div className="nav-item">
                      <a onClick={() => this.QSetView({ page: "chat2" })} className="nav-link " href="#" >
                        Chat
                      </a>
                    </div>

                  {!this.state.userStatus.logged && (
                    <>
                      <div className="nav-item">
                        <a onClick={() => this.QSetView({ page: "signup" })} className="nav-link " href="#">
                          Sign up
                        </a>
                      </div>
                      <div className="nav-item">
                        <a onClick={() => this.QSetView({ page: "login" })} className="nav-link " href="#">
                          Login
                        </a>
                      </div>
                    </>
                   )}
                  </div>
                </div>
              </div>
            </nav>
          </div>

          {/* get the view */}
          <div style={{ margin: "auto" }} id="viewer">
            {this.QGetView(this.state)}
          </div>

        </div>
      </div>
    );
  }
}

export default App;
